import numpy as np
from urllib import request
import cv2

cap = cv2.VideoCapture(0)

target = cv2.imread('target.jpg')
video = cv2.VideoCapture('overlay video.mp4')

detection = False
frameCounter = 0

_, vidFrame = video.read()

hT, wT, cT = target.shape
vidFrame = cv2.resize(vidFrame, (wT, hT))

orb = cv2.ORB_create(nfeatures = 1000)

kp1, des1 = orb.detectAndCompute(target, None)
#target = cv2.drawKeypoints(target, kp1, None)

while True:
	_, camFrame = cap.read()

	kp2, des2 = orb.detectAndCompute(camFrame, None)
	#camFrame = cv2.drawKeypoints(camFrame, kp2, None)

	if detection == False:
		video.set(cv2.CAP_PROP_POS_FRAMES, 0)
		frameCounter = 0
	else:
		if frameCounter == video.get(cv2.CAP_PROP_FRAME_COUNT):
			video.set(cv2.CAP_PROP_POS_FRAMES, 0)
			frameCounter = 0
		_, vidFrame = video.read()
		vidFrame = cv2.resize(vidFrame, (wT, hT))

	bf = cv2.BFMatcher()
	matches = bf.knnMatch(des1, des2, k = 2)
	good_matches = []
	for m, n in matches:
		if m.distance < 0.75 * n.distance:
			good_matches.append(m)
	#print(len(good_matches), end = '\r')
	imgFeatures = cv2.drawMatches(target, kp1, camFrame, kp2, good_matches, None, flags = 2)

	if len(good_matches) > 15:
		detection = True
		srcPts = np.float32([kp1[m.queryIdx].pt for m in good_matches]).reshape(-1, 1, 2)
		dstPts = np.float32([kp2[m.trainIdx].pt for m in good_matches]).reshape(-1, 1, 2)

		matrix, mask = cv2.findHomography(srcPts, dstPts, cv2.RANSAC, 5)

		pts = np.float32([[0, 0], [0, hT], [wT, hT], [wT, 0]]).reshape(-1, 1, 2)
		dst = cv2.perspectiveTransform(pts, matrix)

		# img2 = cv2.polylines(camFrame, [np.int32(dst)], True, (255, 0, 255), 3)

		# cv2.imshow('Bounding Box', img2)

		imgWarp = cv2.warpPerspective(vidFrame, matrix, (camFrame.shape[1], camFrame.shape[0]))

		# cv2.imshow('Warped image', imgWarp)

		mask = np.ones(camFrame.shape[:2], np.uint8)
		mask = cv2.fillPoly(mask, [np.int32(dst)], (0, 0, 0))

		# cv2.imshow('Mask', mask)

		camFrame = cv2.bitwise_and(camFrame, camFrame, mask = mask)
		camFrame = cv2.bitwise_or(camFrame, imgWarp)
	else:
		detection = False

	cv2.imshow("Augmented Reality", camFrame)

	# cv2.imshow('Image Target', target)
	# cv2.imshow('Video frame', vidFrame)
	# cv2.imshow('Camera frame', camFrame)
	# cv2.imshow('Features', imgFeatures)

	if cv2.waitKey(1) & 0xFF == ord('q'):
		break
	frameCounter += 1
