from kivy.app import App
from kivy.uix.image import Image
from kivy.clock import Clock
from kivy.graphics.texture import Texture
import numpy as np
import cv2


class KivyCamera(Image):
    def __init__(self, capture, fps, **kwargs):
        super(KivyCamera, self).__init__(**kwargs)
        self.capture = capture
        self.target = cv2.imread('target.jpg')
        self.video = cv2.VideoCapture('overlay video.mp4')

        self.detection = False
        self.frameCounter = 0

        _, self.vidFrame = self.video.read()

        self.hT, self.wT, self.cT = self.target.shape
        self.vidFrame = cv2.resize(self.vidFrame, (self.wT, self.hT))

        self.orb = cv2.ORB_create(nfeatures = 1000)

        self.kp1, self.des1 = self.orb.detectAndCompute(self.target, None)
        Clock.schedule_interval(self.update, 1.0 / fps)

    def update(self, dt):
        ret, camFrame = self.capture.read()
        if ret:
            kp2, des2 = self.orb.detectAndCompute(camFrame, None)
            #camFrame = cv2.drawKeypoints(camFrame, kp2, None)

            # if self.detection == False:
            #     self.video.set(cv2.CAP_PROP_POS_FRAMES, 0)
            #     self.frameCounter = 0
            # else:
            if self.frameCounter == self.video.get(cv2.CAP_PROP_FRAME_COUNT):
                self.video.set(cv2.CAP_PROP_POS_FRAMES, 0)
                self.frameCounter = 0
            _, self.vidFrame = self.video.read()
            self.vidFrame = cv2.resize(self.vidFrame, (self.wT, self.hT))

            bf = cv2.BFMatcher()
            matches = bf.knnMatch(self.des1, des2, k = 2)
            good_matches = []
            for m, n in matches:
                if m.distance < 0.75 * n.distance:
                    good_matches.append(m)
            #print(len(good_matches), end = '\r')
            imgFeatures = cv2.drawMatches(self.target, self.kp1, camFrame, kp2, good_matches, None, flags = 2)

            if len(good_matches) > 10:
                self.detection = True
                srcPts = np.float32([self.kp1[m.queryIdx].pt for m in good_matches]).reshape(-1, 1, 2)
                dstPts = np.float32([kp2[m.trainIdx].pt for m in good_matches]).reshape(-1, 1, 2)

                matrix, mask = cv2.findHomography(srcPts, dstPts, cv2.RANSAC, 5)

                pts = np.float32([[0, 0], [0, self.hT], [self.wT, self.hT], [self.wT, 0]]).reshape(-1, 1, 2)
                dst = cv2.perspectiveTransform(pts, matrix)

                # img2 = cv2.polylines(camFrame, [np.int32(dst)], True, (255, 0, 255), 3)

                # cv2.imshow('Bounding Box', img2)

                imgWarp = cv2.warpPerspective(self.vidFrame, matrix, (camFrame.shape[1], camFrame.shape[0]))

                # cv2.imshow('Warped image', imgWarp)

                mask = np.ones(camFrame.shape[:2], np.uint8)
                mask = cv2.fillPoly(mask, [np.int32(dst)], (0, 0, 0))

                # cv2.imshow('Mask', mask)

                camFrame = cv2.bitwise_and(camFrame, camFrame, mask = mask)
                camFrame = cv2.bitwise_or(camFrame, imgWarp)
            else:
                self.detection = False
            # convert it to texture
            buf1 = cv2.flip(camFrame, 0)
            buf = buf1.tostring()
            image_texture = Texture.create(
                size=(camFrame.shape[1], camFrame.shape[0]), colorfmt='bgr')
            image_texture.blit_buffer(buf, colorfmt='bgr', bufferfmt='ubyte')
            # display image from the texture
            self.texture = image_texture


class CamApp(App):
    def build(self):
        self.capture = cv2.VideoCapture(0)
        self.my_camera = KivyCamera(capture=self.capture, fps=30)
        return self.my_camera

    def on_stop(self):
        #without this, app will not exit even if the window is closed
        self.capture.release()


if __name__ == '__main__':
    CamApp().run()