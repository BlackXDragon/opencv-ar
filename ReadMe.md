# AR Video Display in Python using OpenCV

The `test.py` file displays the output in a default opencv frame.

The `app.py` file displays the output in a kivy app.

## To try out the application for yourself, follow the steps below:

1. Execute the following commands from the terminal in a directory of your choice to download and setup the project.

It is preferable to use conda and create a new enviroment so that it doesn't interfere with your other Python applications. However, if you prefer some other Python environment manager such as venv, that's fine too!

```
git clone https://gitlab.com/BlackXDragon/opencv-ar.git
cd opencv-ar
conda create -n opencvar pip
conda activate opencvar
pip install -r requirements.txt
```

2. Before we run the code, we need to get our target image and overlay video.

The target image (`target.jpg`) is the image of the object on which the video (`overlay video.mp4`) will be overlayed. The target image needs to contain a rectangular object plane.

There are two ways to prepare the target image:

- Take a photo of a rectangular object which has high contrast (could be a book cover or a quick drawing)
- Get a printout of a high contrast image

The overlay video can be any video you have.

3. Once you have both the files `target.jpg` and `overlay video.mp4` in the root folder of the project, you're ready to run the app. Make sure you're in the conda environment you created in step 1 and then run `python app.py` in your terminal in the root folder

4. The app should open up in a minute or two.

5. The app should overlay the video on top of the target object when you show it in front of the camera!

# Enjoy!